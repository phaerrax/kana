/* window.rs
 *
 * Copyright 2023 Felipe Kinoshita
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use std::collections::HashMap;
use rand::seq::SliceRandom;

use gtk::prelude::*;
use gtk::{gio, glib};
use adw::subclass::prelude::*;
use adw::prelude::AnimationExt;

use gettextrs::gettext;

use crate::config::{APP_ID, VERSION, PROFILE};
use crate::application::Application;
use crate::preferences::PreferencesWindow;
use crate::player::Player;

#[derive(Debug, Copy, Clone)]
pub enum Mode {
    Hiragana,
    Katakana,
}

impl Default for Mode {
    fn default() -> Self {
        Self::Hiragana
    }
}

mod imp {
    use super::*;

    use std::cell::{RefCell, Cell};

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/com/felipekinoshita/Kana/ui/window.ui")]
    pub struct Window {
        pub settings: gio::Settings,

        pub player: Player,

        pub mode: Cell<Mode>,
        pub current_correct_guess: RefCell<String>,
        pub hiragana_map: RefCell<HashMap<String, String>>,
        pub katakana_map: RefCell<HashMap<String, String>>,

        // Template widgets
        #[template_child]
        pub navigation_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        pub status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub container: TemplateChild<gtk::Grid>,
        #[template_child]
        pub guessing_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub play_button: TemplateChild<gtk::Button>,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                settings: gio::Settings::new(APP_ID),

                player: Player::default(),

                mode: Cell::new(Mode::default()),
                current_correct_guess: RefCell::new(String::default()),
                hiragana_map: RefCell::new(HashMap::default()),
                katakana_map: RefCell::new(HashMap::default()),

                navigation_view: TemplateChild::default(),
                status_page: TemplateChild::default(),
                container: TemplateChild::default(),
                guessing_label: TemplateChild::default(),
                play_button: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();

            klass.install_action("win.about", None, move |obj, _, _| {
                obj.show_about_dialog();
            });

            klass.install_action("win.preferences", None, move |obj, _, _| {
                obj.preferences();
            });

            klass.install_action("win.play-audio", None, move |obj, _, _| {
                obj.play_audio_action();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();

            // Devel profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            self.status_page.set_icon_name(Some(APP_ID));

            self.load_kana();
            obj.load_window_size();
        }
    }

    #[gtk::template_callbacks]
    impl Window {
        fn load_kana(&self) {
            let raw_data = gio::File::for_uri("resource:///com/felipekinoshita/Kana/kana.txt")
                .load_contents(None::<&gio::Cancellable>)
                .unwrap()
                .0;
            let data_string = std::str::from_utf8(&raw_data).unwrap();
            let lines: Vec<&str> = data_string.lines().collect();
            let chunks = lines.split(|line| line.is_empty());

            let mut hiragana_map = self.hiragana_map.borrow_mut();
            let mut katakana_map = self.katakana_map.borrow_mut();

            for chunk in chunks {
                let mut chunk_iter = chunk.iter();
                let hiragana = chunk_iter.next().unwrap();
                let katakana = chunk_iter.next().unwrap();
                let romaji = chunk_iter.next().unwrap();

                hiragana_map.insert(hiragana.to_string(), romaji.to_string());
                katakana_map.insert(katakana.to_string(), romaji.to_string());
            }
        }

        fn get_random_values(&self) {
            let mut rng = &mut rand::thread_rng();

            let map = match self.mode.get() {
                Mode::Hiragana => self.hiragana_map.borrow(),
                Mode::Katakana => self.katakana_map.borrow(),
            };

            let keys: Vec<_> = map.keys().collect();
            let random_values: Vec<_> = keys.choose_multiple(&mut rng, 6).cloned().collect();

            if let Some(right_choice) = random_values.choose(&mut rng) {
                self.current_correct_guess.replace(map.get(*right_choice).unwrap().to_string());
                self.guessing_label.set_label(map.get(*right_choice).unwrap());
            }

            self.container.observe_children()
                .into_iter()
                .enumerate()
                .for_each(|(index, child)| {
                    if let Ok(child) = child {
                        let button = child.downcast_ref::<gtk::Button>().unwrap();

                        button.set_can_target(true);
                        button.set_can_focus(true);

                        button.remove_css_class("suggested-action");
                        button.remove_css_class("destructive-action");
                        button.add_css_class("flat");

                        button.set_label(random_values[index]);
                    }
                });

            if self.settings.boolean("play-sound-automatically") {
                self.play_audio();
            }

            self.play_button.grab_focus();
        }

        #[template_callback]
        fn on_hiragana_button_clicked(&self, _button: gtk::Button) {
            self.mode.replace(Mode::Hiragana);

            self.get_random_values();

            self.navigation_view.push_by_tag("main");
        }

        #[template_callback]
        fn on_katakana_button_clicked(&self, _button: gtk::Button) {
            self.mode.replace(Mode::Katakana);

            self.get_random_values();

            self.navigation_view.push_by_tag("main");
        }

        #[template_callback]
        fn on_card_clicked(&self, button: gtk::Button) {
            let map = match self.mode.get() {
                Mode::Hiragana => self.hiragana_map.borrow(),
                Mode::Katakana => self.katakana_map.borrow(),
            };

            self.container.observe_children()
                .into_iter()
                .for_each(|child| {
                    if let Ok(child) = child {
                        let button = child.downcast_ref::<gtk::Button>().unwrap();
                        let label = button.label().unwrap().to_string();

                        // highlight button if it's the correct guess
                        if let Some(kana) = self.find_key_for_value(&map, self.current_correct_guess.borrow().to_string()) {
                            if label == kana {
                                button.remove_css_class("flat");
                                button.add_css_class("suggested-action");
                            }
                        }

                        // make all buttons not receive input
                        button.set_can_target(false);
                        button.set_can_focus(false);
                    }
                });

            let button_label = button.label().unwrap();
            let guess = map.get_key_value(&button_label.to_string())
                .unwrap()
                .1;

            button.remove_css_class("flat");

            if *guess == self.current_correct_guess.borrow().to_string() {
                button.add_css_class("suggested-action");
            } else {
                button.add_css_class("destructive-action");
            }

            // animating nothing to nothing
            // using is as a delay
            let target = adw::PropertyAnimationTarget::new(&button, "opacity");
            let animation = adw::TimedAnimation::new(&button, 1.0, 1.0, 1300, target);
            animation.set_follow_enable_animations_setting(false);
            animation.play();

            animation.connect_done(glib::clone!(@weak self as this => move |_| {
                this.player.stop();
                this.get_random_values();
            }));
        }

        #[template_callback]
        fn on_play_button_clicked(&self, _button: gtk::Button) {
            self.play_audio();
        }

        pub fn play_audio(&self) {
            let current_correct_guess = self.current_correct_guess.borrow();
            self.player.play(current_correct_guess.to_string());
        }

        fn find_key_for_value(&self, map: &HashMap<String, String>, value: String) -> Option<String> {
            map.iter()
                .find_map(|(key, &ref val)| if *val == value { Some(key) } else { None }).cloned()
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        fn close_request(&self) -> glib::Propagation {
            let window = self.obj();

            if let Err(err) = window.save_window_size() {
                log::error!("Failed to save window state: {}", &err);
            }

            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Window {
    pub fn new(application: &Application) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }

    fn play_audio_action(&self) {
        let imp = self.imp();

        imp.play_button.activate();
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let imp = self.imp();

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        Ok(())
    }

    fn load_window_size(&self) {
        let imp = self.imp();

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");

        self.set_default_size(width, height);
    }

    fn show_about_dialog(&self) {
        let dialog = adw::AboutWindow::builder()
            .application_icon(APP_ID)
            .application_name(gettext("Kana"))
            .license_type(gtk::License::Gpl30)
            .comments(gettext("Learn Japanese characters"))
            .website("https://gitlab.gnome.org/fkinoshita/Kana")
            .issue_url("https://gitlab.gnome.org/fkinoshita/Kana/-/issues/new")
            .version(VERSION)
            .transient_for(self)
            .translator_credits(gettext("translator-credits"))
            .developer_name("Felipe Kinoshita")
            .developers(["Felipe Kinoshita https://felipekinoshita.com"])
            .artists(["Brage Fuglseth https://bragefuglseth.dev"])
            .copyright("© 2023 Felipe Kinoshita.")
            .release_notes_version(VERSION)
            .release_notes("<p>Small bugfix release:</p><ul><li>Fix some guesses not getting highlighted</li></ul><p>If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out!</p>")
            .build();

        dialog.present();
    }

    fn preferences(&self) {
        let dialog = PreferencesWindow::new();
        dialog.set_transient_for(Some(self));

        dialog.present();
    }
}

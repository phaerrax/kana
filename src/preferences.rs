/* window.rs
 *
 * Copyright 2023 Felipe Kinoshita
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use gtk::{gio, glib};
use adw::subclass::prelude::*;

use crate::config::{APP_ID, PROFILE};

mod imp {
    use super::*;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/com/felipekinoshita/Kana/ui/preferences.ui")]
    pub struct PreferencesWindow {
        pub settings: gio::Settings,

        // Template widgets
        #[template_child]
        pub play_sound_automatically_row: TemplateChild<adw::SwitchRow>,
    }

    impl Default for PreferencesWindow {
        fn default() -> Self {
            Self {
                settings: gio::Settings::new(APP_ID),

                play_sound_automatically_row: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PreferencesWindow {
        const NAME: &'static str = "PreferencesWindow";
        type Type = super::PreferencesWindow;
        type ParentType = adw::PreferencesWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PreferencesWindow {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();

            // Devel profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.load_state();
        }
    }

    #[gtk::template_callbacks]
    impl PreferencesWindow {
        #[template_callback]
        fn on_play_sound_toggled(&self, _pspec: &glib::ParamSpec, row: &adw::SwitchRow) {
            if let Err(err) = self.settings.set_boolean("play-sound-automatically", row.is_active()) {
                log::error!("Failed to save 'play-sound-automatically' state: {}", &err);
            }
        }
    }

    impl WidgetImpl for PreferencesWindow {}
    impl WindowImpl for PreferencesWindow {}
    impl AdwWindowImpl for PreferencesWindow {}
    impl PreferencesWindowImpl for PreferencesWindow {}
}

glib::wrapper! {
    pub struct PreferencesWindow(ObjectSubclass<imp::PreferencesWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::PreferencesWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl PreferencesWindow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    fn load_state(&self) {
        let imp = self.imp();

        self.bind_flag("play-sound-automatically", &imp.play_sound_automatically_row);
    }

    fn bind_flag(&self, setting_name: &str, row: &TemplateChild<adw::SwitchRow>) {
        let imp = self.imp();

        imp.settings.bind(setting_name, &**row, "active").build();
    }
}
